#ifndef _GANGLIA_DRIVER_LIB_DRIVER_H_
#define _GANGLIA_DRIVER_LIB_DRIVER_H_

#include <ros/ros.h>
#include <vector>
#include <iostream>
#include <string.h>

#include "../../3rdparty/libmodbus/modbus.h"

class GangliaDriver
{
public:
    GangliaDriver();
    ~GangliaDriver();

    bool        init(std::string port_name = "/dev/ttyUSB0", uint32_t baud_rate = 2000000, char parity = 'N', int data_bit = 8, int stop_bit = 1);

    bool        setByteTimeout(uint32_t byte_timeout_sec, uint32_t byte_timeout_usec);
    bool        setResponseTimeout(uint32_t response_timeout_sec, uint32_t response_timeout_usec);

    // bool        setId(uint8_t id, uint8_t new_id);

    // bool        setModelName(uint8_t id, std::string name);
    // std::string getModelName(uint8_t id);

    // bool        scan(uint8_t *get_id, uint8_t *get_id_num, uint8_t range = 200);
    // bool        ping(uint8_t id, uint16_t *get_model_number);

    // bool        reboot(uint8_t id);
    // bool        reset(uint8_t id);

    //Connection Function
    bool        connect();
    bool        disconnect();

    bool        writeBit(uint8_t id, int addr, uint8_t value);
    bool        writeBits(uint8_t id, int addr, int nb, uint8_t *value);
    bool        readBits(uint8_t id, int addr, int nb, uint8_t *dest);
    bool        readInputBits(uint8_t id, int addr, int nb, uint8_t *dest);
    bool        writeRegister(uint8_t id, int addr, int value);
    bool        writeRegisters(uint8_t id, int addr, int nb, uint16_t *value);
    bool        readRegisters(uint8_t id, int addr, int nb, uint16_t *dest);
    bool        readInputRegisters(uint8_t id, int addr, int nb, uint16_t *dest);
    bool        writeReadRegisters(uint8_t id, int write_addr, int write_nb, uint16_t* value, int read_addr, int read_nb, uint16_t *dest);

private:
    bool        selectId(int id);

private:
    modbus_t        *modbus_;
    std::string     port_name_;
    uint32_t        baud_rate_;
    char            parity_;
    int             data_bit_;
    int             stop_bit_;

};

#endif //_GANGLIA_DRIVER_LIB_DRIVER_H_