#ifndef _GANGLIA_ITEM_H_
#define _GANGLIA_ITEM_H_

#include <stdint.h>
#include <stddef.h>

#define GANGLIA_VERSION_1 1
#define ZLACD_VERSION_1   3
#define GANGLIA_VERSION_5 5


#define MOTOR_BLDC_VALUE 0x05
#define CURRENT_CONTROL_MODE_VALUE 0xFFFD
#define VELOCITY_CONTROL_MODE_VALUE 0xFFFE
#define POSITION_CONTROL_MODE_VALUE 0xFFFF
#define NO_CONTROL_MODE_VALUE 0x0000
#define PROFILE_VELOCITY_MODE_VALUE 0x0001
#define PROFILE_POSITION_MODE_VALUE 0x0002
#define HOMING_MODE_VALUE 0x0006
#define PWM_FIXED_MODE_VALUE 0x0007
#define PWM_TEST_MODE_VALUE 0x0008
#define MOTOR_START_TRUE_VALUE 0xFF00
#define MOTOR_START_FALSE_VALUE 0x0000

#define BYTE  1
#define WORD  2
#define DWORD 4

// test

namespace GangliaItem
{

enum ItemType { AnalogOutput, DigitalOutput, AnalogInput, DigitalInput };
typedef struct 
{
    const char* item_name;
    ItemType    item_type;
    uint16_t    address;
    uint8_t     item_name_length;
    uint8_t     data_length; //WORD
} ControlItem;

typedef struct
{
    bool flag;
} ModelInfo;

const ControlItem* getControlTable(uint16_t model_number);
const ModelInfo* getModelInfo(uint16_t model_number);

uint8_t getTheNumberOfControlItem();

}

#endif //_GANGLIA_ITEM_H_