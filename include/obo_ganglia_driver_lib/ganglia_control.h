#ifndef _GANGLIA_CONTROL_H_
#define _GANGLIA_CONTROL_H_

#include "obo_ganglia_driver_lib/ganglia_driver.h"
#include "obo_ganglia_driver_lib/ganglia_item.h"

class GangliaControl : public GangliaDriver
{
public:
    #define CONTROL_TABLE_ITEM_SIZE 30
    
    enum OperationMode { CurrentControl=-3, 
                        VelocityControl, 
                        PositionControl, 
                        NoControl,
                        ProfileVelocity,
                        ProfilePosition,
                        None3,
                        None4,
                        None5,
                        HomingMode,
                        PWMFixed,
                        PWMTest };

    struct GangliaState
    {
        int32_t encoder;
        int16_t encoder_abs;
        int32_t velocity;
        int32_t current;

        friend std::ostream& operator<< (std::ostream& os, const GangliaState& data)
        {
            return os << data.encoder << " " << data.encoder_abs << " " << data.velocity << " " << data.current;
        }
    };

    GangliaControl();
    ~GangliaControl();

    bool setTimeout(uint32_t byte_timeout_sec, uint32_t byte_timeout_usec, uint32_t response_timeout_sec, uint32_t response_timeout_usec);

    // bool setModelName(uint8_t id, std::string name);
    // std::string getModelName(uint8_t id);

    // bool scan(uint8_t *get_id, uint8_t *get_id_num, uint8_t range = 200);
    bool ping(uint8_t id, uint16_t *get_model_number = 0);

    // bool reboot(uint8_t id);
    // bool reset(uint8_t id);
    bool isInitialized(uint8_t id, bool *value);

    // bool setReadAbsEncoder(uint8_t id, bool value);
    bool getBLDCMode(uint8_t id, bool *value);
    bool setBLDCMode(uint8_t id, bool value);
    // bool setMotorEncoder(uint8_t id, bool value);
    bool getDriveEnable(uint8_t id, bool *value);
    bool setDriveEnable(uint8_t id, bool value);
    bool getControlEnable(uint8_t id, bool *value);
    bool setControlEnable(uint8_t id, bool value);
    // bool setMotorStop(uint8_t id, bool value);

    bool getOperationMode(uint8_t id, int16_t *value);
    bool setCurrentControlMode(uint8_t id);
    bool setVelocityControlMode(uint8_t id);
    bool setPositionControlMode(uint8_t id);
    bool setPwmFixedMode(uint8_t id);

    // bool setCurrentPID(uint8_t id, uint32_t p, uint32_t i, uint32_t d);
    // bool setVelocityPID(uint8_t id, uint32_t p, uint32_t i, uint32_t d);
    // bool setPositionPID(uint8_t id, uint32_t p, uint32_t i, uint32_t d);
    // bool setPositionGainP(uint8_t id, float value);
    // bool setPositionGainI(uint8_t id, float value);
    // bool setPositionGainD(uint8_t id, float value);
    // bool setPositionLimitI(uint8_t id, int16_t value);

    bool setCurrentTarget(uint8_t id, int32_t value);
    bool setVelocityTarget(uint8_t id, int32_t value);
    bool setPositionTarget(uint8_t id, int32_t value);
    bool setPwmTarget(uint8_t id, int16_t value);

    bool getState(uint8_t id, GangliaState *state, bool read_abs_encoder);

    bool z_setVelovityMode(uint8_t id);
    bool z_setAccTime(uint8_t id, uint16_t value);
    bool z_setDecTime(uint8_t id, uint16_t value);
    bool z_setEnable(uint8_t id);
    bool z_setVelocityTarget_L(uint8_t id, int16_t value);
    bool z_setVelocityTarget_R(uint8_t id, int16_t value);
    bool z_stop(uint8_t id);
    bool z_start(uint8_t id);
    bool z_getVelocity(uint8_t id, int16_t* value);
    bool z_getCurrent(uint8_t id, int16_t* value);
    bool z_getMotorTemp(uint8_t id, int8_t* value);
    bool z_getDriverTemp(uint8_t id, int16_t* value);
    bool z_checkALM(uint8_t id, uint16_t* value);
    



private:
    bool  initControlTable(uint32_t model_number);
    const GangliaItem::ControlItem* getControlItem(const char* item_name);

private:
    const GangliaItem::ControlItem *control_table_;
    const GangliaItem::ModelInfo *model_info_;
    uint16_t the_number_of_control_item_;
    
};

#endif //_GANGLIA_CONTROL_H_