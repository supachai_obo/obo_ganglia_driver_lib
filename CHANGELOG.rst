^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package obo_ganglia_driver_lib
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.0 (2020-3-20)
------------------
* Add set pwm target funtion
* Add set pwm fixed mode function 
* Change ganglia state return value to pointer
2019-8-5
* Add check Ganglia initialized function
* Add read input bit function
2019-7-24
* Add motor control enable getter function
* Add motor drive enable getter function
* Add motor bldc mode getter function
* Change function arguments to pointer
2019-4-17
* Add .gitignore
* Change Ganglia state struct location
* Change class control to inheritance of driver
* Add read and write holding register function
* Update Ganglia class structure
2018-6-14
* Add test node for Ganglia 
* Add Ganglia control class
* Add Ganglia driver class 
* Initial commit