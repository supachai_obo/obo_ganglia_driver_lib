#include "obo_ganglia_driver_lib/ganglia_item.h"

namespace GangliaItem
{

//=========================================================
// Ganglia register definitions
//=========================================================

// onboard analog output register
static const char r_Position_Target[]   = "Position_Target";
static const char r_Velocity_Target[]   = "Velocity_Target";
static const char r_Current_Target[]    = "Current_Target";
static const char r_PWM_Value[]         = "PWM_Value";
static const char r_Operation_Mode[]    = "Operation_Mode";
// onboard digital output register
static const char r_Control_Enable[]    = "Control_Enable";
static const char r_BLDC_Mode[]         = "BLDC_Mode";
static const char r_Drive_Enable[]      = "Drive_Enable";
// onboard analog input register
static const char r_Absolute_Encoder[]  = "Absolute_Encoder";
// onboard digital input register
static const char r_Error_0[]           = "Error_0";


// self-created register
static const char sr_Motor_State[]      = "Motor_State";

static const ControlItem items_GV1[]{
    {r_Position_Target,     GangliaItem::AnalogOutput,   0x0008, sizeof(r_Position_Target) - 1,  2},
    {r_Velocity_Target,     GangliaItem::AnalogOutput,   0x0018, sizeof(r_Velocity_Target) - 1,  2},
    {r_Current_Target,      GangliaItem::AnalogOutput,   0x0028, sizeof(r_Current_Target) - 1,   2},
    {r_PWM_Value,           GangliaItem::AnalogOutput,   0x0050, sizeof(r_PWM_Value) - 1,        1},
    {r_Operation_Mode,      GangliaItem::AnalogOutput,   0x0080, sizeof(r_Operation_Mode) - 1,   1},
    {r_Control_Enable,      GangliaItem::DigitalOutput,  0x0003, sizeof(r_Control_Enable) - 1,   1},
    {r_BLDC_Mode,           GangliaItem::DigitalOutput,  0x0008, sizeof(r_BLDC_Mode) - 1,        1},
    {r_Drive_Enable,        GangliaItem::DigitalOutput,  0x000D, sizeof(r_Drive_Enable) - 1,     1},
    {r_Absolute_Encoder,    GangliaItem::DigitalInput,   0x0021, sizeof(r_Absolute_Encoder) - 1, 1},
    {sr_Motor_State,        GangliaItem::AnalogInput,    0x0000, sizeof(sr_Motor_State) - 1,     6},
    {r_Error_0,             GangliaItem::DigitalInput,   0x0000, sizeof(r_Error_0) - 1,          1},
};

static const char r_Control_Word[]           = "Control_Word";
static const char r_Control_Mode[]           = "Control_Mode";
static const char r_Acceleration_Time_L[]    = "Acceleration_Time_L";
static const char r_Acceleration_Time_R[]    = "Acceleration_Time_R";
static const char r_Dcceleration_Time_L[]    = "Dcceleration_Time_L";
static const char r_Dcceleration_Time_R[]    = "Dcceleration_Time_R";
static const char r_Target_Velocity_L[]      = "Target_Velocity_L";
static const char r_Target_Velocity_R[]      = "Target_Velocity_R";
static const char r_Actual_Velocity_L[]      = "Actual_Velocity_L";
static const char r_Actual_Velocity_R[]      = "Actual_Velocity_R";
static const char r_Actual_Current_L[]       = "Actual_Current_L";
static const char r_Actual_Current_R[]       = "Actual_Current_R";
static const char r_Motor_Temp[]             = "Motor_Temp";
static const char r_Driver_Temp[]            = "Driver_Temp";
static const char r_Check_ALM_L[]            = "Check_ALM_L";
static const char r_Check_ALM_R[]            = "Check_ALM_R";




static const ControlItem items_ZLACD[]{
    {r_Control_Word ,               GangliaItem::AnalogOutput,   0x200E, sizeof(r_Control_Word) - 1,  2},
    {r_Control_Mode ,               GangliaItem::AnalogOutput,   0x200D, sizeof(r_Control_Mode) - 1,  2},
    {r_Acceleration_Time_L ,        GangliaItem::AnalogOutput,   0x2080, sizeof(r_Acceleration_Time_L) - 1,  2},
    {r_Acceleration_Time_R ,        GangliaItem::AnalogOutput,   0x2081, sizeof(r_Acceleration_Time_R) - 1,  2},
    {r_Dcceleration_Time_L ,        GangliaItem::AnalogOutput,   0x2082, sizeof(r_Dcceleration_Time_L) - 1,  2},
    {r_Dcceleration_Time_R ,        GangliaItem::AnalogOutput,   0x2083, sizeof(r_Dcceleration_Time_R) - 1,  2},
    {r_Target_Velocity_L ,          GangliaItem::AnalogOutput,   0x2088, sizeof(r_Target_Velocity_L) - 1,  2},
    {r_Target_Velocity_R ,          GangliaItem::AnalogOutput,   0x2089, sizeof(r_Target_Velocity_R) - 1,  2},
    {r_Actual_Velocity_L ,          GangliaItem::AnalogOutput,   0x20AB, sizeof(r_Actual_Velocity_L) - 1,  2},
    {r_Actual_Velocity_R ,          GangliaItem::AnalogOutput,   0x20AC, sizeof(r_Actual_Velocity_R) - 1,  2},
    {r_Actual_Current_R ,           GangliaItem::AnalogOutput,   0x20AD, sizeof(r_Actual_Velocity_R) - 1,  2},
    {r_Actual_Current_L ,           GangliaItem::AnalogOutput,   0x20AE, sizeof(r_Actual_Velocity_R) - 1,  2},
    {r_Motor_Temp ,                 GangliaItem::AnalogOutput,   0x20A4, sizeof(r_Motor_Temp) - 1,  2},
    {r_Driver_Temp ,                GangliaItem::AnalogOutput,   0x20B0, sizeof(r_Driver_Temp) - 1, 2},
    {r_Check_ALM_L ,                GangliaItem::AnalogOutput,   0x20A5, sizeof(r_Check_ALM_L) - 1,  2},
    {r_Check_ALM_R ,                GangliaItem::AnalogOutput,   0x20A6, sizeof(r_Check_ALM_R) - 1,  2},
     

};

#define COUNT_GV1_ITEMS (sizeof(items_GV1) / sizeof(items_GV1[0]))
#define COUNT_ZLACD_ITEMS (sizeof(items_ZLACD) / sizeof(items_ZLACD[0]))

static const ModelInfo info_GV1 = {true};
static const ModelInfo info_ZLACDV1 = {true};

//=========================================================
// Get Ganglia control table for the specified ganglia type
//=========================================================
static uint8_t the_number_of_item = 0;

const ControlItem* getControlTable(uint16_t model_number)
{
    const ControlItem *control_table;
    if(model_number == GANGLIA_VERSION_1)
    {
        control_table = items_GV1;
        the_number_of_item = COUNT_GV1_ITEMS;
    }

    if(model_number == ZLACD_VERSION_1)
    {
        control_table = items_ZLACD;
        the_number_of_item = COUNT_ZLACD_ITEMS;
    }

    else
    {
        control_table = NULL;
        the_number_of_item = 0;
    }
    return control_table;
}

const ModelInfo* getModelInfo(uint16_t model_number)
{
    const ModelInfo *info;
    if (model_number == GANGLIA_VERSION_1)
    {
        info = &info_GV1;
    }
    else if (model_number == ZLACD_VERSION_1)
    {
        info = &info_ZLACDV1;
    }

    else
    {
        info = NULL;
    }

    return info;
}

uint8_t getTheNumberOfControlItem()
{
  return the_number_of_item;
}

}
/*
static const ControlItem items_GV1[]{
    {r_Position_Target,     GangliaItem::AnalogOutput,   0x0008, sizeof(r_Position_Target) - 1,  2},
    {r_Velocity_Target,     GangliaItem::AnalogOutput,   0x0018, sizeof(r_Velocity_Target) - 1,  2},
    {r_Current_Target,      GangliaItem::AnalogOutput,   0x0028, sizeof(r_Current_Target) - 1,   2},
    {r_PWM_Value,           GangliaItem::AnalogOutput,   0x0050, sizeof(r_PWM_Value) - 1,        1},
    {r_Operation_Mode,      GangliaItem::AnalogOutput,   0x0080, sizeof(r_Operation_Mode) - 1,   1},
    {r_Control_Enable,      GangliaItem::DigitalOutput,  0x0003, sizeof(r_Control_Enable) - 1,   1},
    {r_BLDC_Mode,           GangliaItem::DigitalOutput,  0x0008, sizeof(r_BLDC_Mode) - 1,        1},
    {r_Drive_Enable,        GangliaItem::DigitalOutput,  0x000D, sizeof(r_Drive_Enable) - 1,     1},
    {r_Absolute_Encoder,    GangliaItem::DigitalInput,   0x0021, sizeof(r_Absolute_Encoder) - 1, 1},
    {sr_Motor_State,        GangliaItem::AnalogInput,    0x0000, sizeof(sr_Motor_State) - 1,     6},
    {r_Error_0,             GangliaItem::DigitalInput,   0x0000, sizeof(r_Error_0) - 1,          1},
};

*/
