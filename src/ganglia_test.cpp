#include <iostream>
#include <ros/ros.h>
// #include <../../3rdparty/libmodbus/modbus.h>
#include "obo_ganglia_driver_lib/ganglia_control.h"
// #include "obo_ganglia_driver_lib/structs.h"
//#include "obo_ganglia_driver_lib/conio.h"
#include <pthread.h>
#include <std_msgs/String.h>

static volatile bool keep_running = true;

static void* userInput_thread(void*)
{
       while(keep_running) {
           if (std::cin.get() == 'q')
           {
               //! desired user input 'q' received
               keep_running = false;
               std::cout << "keep_running : " << (int)keep_running  << std::endl;
               break;
           }
       }
       keep_running = false;
       std::cout << "out of thread loop "  << std::endl;
}

void keyboardCallback(const std_msgs::String::ConstPtr& msg)
{
    const char* key_in = msg->data.c_str();
    char keyboard = *key_in;
    //std::cout << keyboard << std::endl;
    ROS_INFO("I heard: [%s]", key_in);
    if (keyboard == 'q')
    {
        ROS_INFO("Stop !!!");
        keep_running = false;
        ROS_INFO("keep_running %i", keep_running );
    }
}

int main(int argc, char **argv)
{
    //set up thread
    pthread_t tId;
    //(void) pthread_create(&tId, 0, userInput_thread, 0);

    ros::init(argc, argv, "ganglia_test");
    GangliaControl zlac;
    ros::NodeHandle nh;
    ros::Rate loop_rate(10);

    zlac.init("/dev/ttyUSB0", 115200,'N', 8, 1);
    if(!zlac.connect())
        return -1;
    
    ros::Subscriber sub = nh.subscribe("keys",1000, keyboardCallback);

    int input;
    //input = 1;
    std::cout << "Please input 1: \n";
    std::cin >> input;
    
    if(input == 1)
    {
        
        int zlac_id = 1;
        int16_t zlac_vel[2] = {0, 0};
        int8_t zlac_w_temp[2] = {0, 0};
        int16_t zlac_d_temp = 0;
        int16_t zlac_current[2] = {0, 0};
        
        //std::cout << "b4 main loop "  << std::endl;

        if (!zlac.z_setVelovityMode(zlac_id)){
            std::cout << "set velocity mode failed\n";
        }

        if (!zlac.z_setEnable(zlac_id)){
            std::cout << "set enable failed\n";
        }
        
        
        int n = 0;

        while(ros::ok() && keep_running) // ros::ok()
        {

            if (!zlac.z_setVelocityTarget_L(zlac_id,50)){
                std::cout << "set velocity L failed\n";
            }
            

            if (!zlac.z_setVelocityTarget_R(zlac_id,50)){
                std::cout << "set velocity R failed\n";
            }
            
            //ROS_INFO("keep_running in while %i", keep_running );
            //n = n+1; 

                
            std::cout <<"enter reading state" << std::endl;
                
            if (!zlac.z_getVelocity(zlac_id, zlac_vel)){
                std::cout << "get velocity failed" << std::endl;
            }
            
            std::cout << "velocity "<< zlac_vel[0] << " / " << zlac_vel[1] << std::endl;
            
            if (!zlac.z_getMotorTemp(zlac_id, zlac_w_temp)){
                std::cout << "get motor temp failed" << std::endl;
            }

            std::cout << "motor temp "<< zlac_w_temp[0] << " / " << zlac_w_temp[1] << std::endl;
            
            if (!zlac.z_getDriverTemp(zlac_id, &zlac_d_temp)){
                std::cout << "get driver temp failed" << std::endl;
            }
                
            std::cout << "driver temp "<< zlac_d_temp << std::endl;
            
            if (!zlac.z_getCurrent(zlac_id, zlac_current)){
                std::cout << "get current failed" << std::endl;
            }
            std::cout << "current "<< zlac_current[0] << " / " << zlac_current[1] << std::endl;
            
            /*
            }

            if (!zlac.z_stop(zlac_id)){
                std::cout << "stop failed"  << std::endl;
            }
            */
                
            ros::spinOnce();
            loop_rate.sleep();
        }

        std::cout << "out of while loop"  << std::endl;

        if (!zlac.z_stop(zlac_id)){
            std::cout << "stop failed"  << std::endl;
        }
    }
    
    //(void) pthread_join(tId, NULL);

    return 0;
}