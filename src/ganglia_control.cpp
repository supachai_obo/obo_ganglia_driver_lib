#include "obo_ganglia_driver_lib/ganglia_control.h"

GangliaControl::GangliaControl():
    the_number_of_control_item_(0)
{
    initControlTable(ZLACD_VERSION_1);
}

GangliaControl::~GangliaControl()
{
}

bool GangliaControl::setTimeout(uint32_t byte_timeout_sec, uint32_t byte_timeout_usec, uint32_t response_timeout_sec, uint32_t response_timeout_usec)
{
    bool ret = true;
    ret &= setByteTimeout(byte_timeout_sec, byte_timeout_usec);
    ret &= setResponseTimeout(response_timeout_sec, response_timeout_usec);
    return ret;
}

// bool GangliaControl::setId(uint8_t id, uint8_t new_id)
// {

// }

// bool setModelName(uint8_t id, std::string name);
// std::string getModelName(uint8_t id);

// bool scan(uint8_t *get_id, uint8_t *get_id_num, uint8_t range = 200);
bool GangliaControl::ping(uint8_t id, uint16_t *get_model_number)
{
    bool ret = true;
    // isOK = driver_.ping(id, get_model_number);
    const GangliaItem::ControlItem* motor_start = getControlItem("Control_Enable");
    std::vector<uint8_t> dest(motor_start->data_length);
    ret = readBits(id, motor_start->address, motor_start->data_length, (uint8_t *)&dest[0]);
    return ret;
}
// bool reboot(uint8_t id);
// bool reset(uint8_t id);

// bool GangliaControl::setReadAbsEncoder(uint8_t id, bool value)
// {

// }

bool GangliaControl::isInitialized(uint8_t id, bool *value)
{
    bool ret = true;
    const GangliaItem::ControlItem* error_0 = getControlItem("Error_0");
    std::vector<uint8_t> dest(error_0->data_length);
    ret = readInputBits(id, error_0->address, error_0->data_length, (uint8_t *)&dest[0]);
    *value = !dest[0];
    std::cout << "GangliaControl::isInitialized(" << (int)id << ") : " << *value << std::endl;
    return ret;
}

bool GangliaControl::getBLDCMode(uint8_t id, bool *value)
{
    bool ret = true;
    const GangliaItem::ControlItem* motor_bldc = getControlItem("BLDC_Mode");
    std::vector<uint8_t> dest(motor_bldc->data_length);
    ret = readBits(id, motor_bldc->address, motor_bldc->data_length, (uint8_t *)&dest[0]);
    *value = dest[0];
    std::cout << "GangliaControl::getBLDCMode(" << (int)id << ") : " << *value << std::endl;
    return ret;
}

bool GangliaControl::setBLDCMode(uint8_t id, bool value)
{
    std::cout << "GangliaControl::setBLDCMode(" << (int)id << ") : " << value << std::endl;
    const GangliaItem::ControlItem* motor_bldc = getControlItem("BLDC_Mode");
    return writeBit(id, motor_bldc->address, value);
}

bool GangliaControl::getDriveEnable(uint8_t id, bool *value)
{
    bool ret = true;
    const GangliaItem::ControlItem* motor_enable = getControlItem("Drive_Enable");
    std::vector<uint8_t> dest(motor_enable->data_length);
    ret = readBits(id, motor_enable->address, motor_enable->data_length, (uint8_t *)&dest[0]);
    *value = dest[0];
    std::cout << "GangliaControl::getDriveEnable(" << (int)id << ") : " << *value << std::endl;
    value = (bool *)&dest[0];
    return ret;
}

bool GangliaControl::setDriveEnable(uint8_t id, bool value)
{
    std::cout << "GangliaControl::setDriveEnable(" << (int)id << ") : " << value << std::endl;
    const GangliaItem::ControlItem* motor_enable = getControlItem("Drive_Enable");
    return writeBit(id, motor_enable->address, value);
}

bool GangliaControl::getControlEnable(uint8_t id, bool *value)
{
    bool ret = true;
    const GangliaItem::ControlItem* motor_start = getControlItem("Control_Enable");
    std::vector<uint8_t> dest(motor_start->data_length);
    ret = readBits(id, motor_start->address, motor_start->data_length, (uint8_t *)&dest[0]);
    *value = dest[0];
    std::cout << "GangliaControl::getControlEnable(" << (int)id << ") : " << *value << std::endl;
    value = (bool *)&dest[0];
    return ret;
}

bool GangliaControl::setControlEnable(uint8_t id, bool value)
{
    std::cout << "GangliaControl::setControlEnable(" << (int)id << ") : " << value << std::endl;
    const GangliaItem::ControlItem* motor_start = getControlItem("Control_Enable");
    return writeBit(id, motor_start->address, value);
}

// bool GangliaControl::setMotorStop(uint8_t id, bool value);

bool GangliaControl::getOperationMode(uint8_t id, int16_t *value)
{
    bool ret = true;
    const GangliaItem::ControlItem* control_mode = getControlItem("Operation_Mode");
    std::vector<uint16_t> dest(control_mode->data_length);
    ret = readRegisters(id, control_mode->address, control_mode->data_length, (uint16_t *)&dest[0]);
    *value = dest[0];
    std::cout << "GangliaControl::getOperationMode(" << (int)id << ") : " << *value << std::endl;
    return ret;
}

bool GangliaControl::setCurrentControlMode(uint8_t id)
{
    std::cout << "GangliaControl::setCurrentControlMode(" << (int)id << ")" << std::endl;
    const GangliaItem::ControlItem* control_mode = getControlItem("Operation_Mode");
    return writeRegister(id, control_mode->address, CURRENT_CONTROL_MODE_VALUE);
}

bool GangliaControl::setVelocityControlMode(uint8_t id)
{
    std::cout << "GangliaControl::setVelocityControlMode(" << (int)id << ")" << std::endl;
    const GangliaItem::ControlItem* control_mode = getControlItem("Operation_Mode");
    return writeRegister(id, control_mode->address, VELOCITY_CONTROL_MODE_VALUE);
}

bool GangliaControl::setPositionControlMode(uint8_t id)
{
    std::cout << "GangliaControl::setPositionControlMode(" << (int)id << ")" << std::endl;
    const GangliaItem::ControlItem* control_mode = getControlItem("Operation_Mode");
    return writeRegister(id, control_mode->address, POSITION_CONTROL_MODE_VALUE);
}

bool GangliaControl::setPwmFixedMode(uint8_t id)
{
    std::cout << "GangliaControl::setPwmFixedMode(" << (int)id << ")" << std::endl;
    const GangliaItem::ControlItem* control_mode = getControlItem("Operation_Mode");
    return writeRegister(id, control_mode->address, PWM_FIXED_MODE_VALUE);
}

// bool GangliaControl::setPositionGainP(uint8_t id, float value)
// bool GangliaControl::setPositionGainI(uint8_t id, float value)
// bool GangliaControl::setPositionGainD(uint8_t id, float value)
// {

// }

// bool GangliaControl::setPositionLimitI(uint8_t id, int16_t value)
// {
//     const GangliaItem::ControlItem* current_target = getControlItem("Position_Limit_I");
//     return writeRegisters(id, current_target->address, current_target->data_length, value);

// }

bool GangliaControl::setCurrentTarget(uint8_t id, int32_t value)
{
    const GangliaItem::ControlItem* current_target = getControlItem("Current_Target");
    return writeRegisters(id, current_target->address, current_target->data_length, (uint16_t *)&value);
}

bool GangliaControl::setVelocityTarget(uint8_t id, int32_t value)
{
    const GangliaItem::ControlItem* velocity_target = getControlItem("Velocity_Target");
    return writeRegisters(id, velocity_target->address, velocity_target->data_length, (uint16_t *)&value);
}

bool GangliaControl::setPositionTarget(uint8_t id, int32_t value)
{
    const GangliaItem::ControlItem* position_target = getControlItem("Position_Target");
    return writeRegisters(id, position_target->address, position_target->data_length, (uint16_t *)&value);
}

bool GangliaControl::setPwmTarget(uint8_t id, int16_t value)
{
    const GangliaItem::ControlItem* pwm_target = getControlItem("PWM_Value");
    return writeRegisters(id, pwm_target->address, pwm_target->data_length, (uint16_t *)&value);
}

bool GangliaControl::getState(uint8_t id, GangliaState *state, bool read_abs_encoder)
{
    bool ret = true;
    const GangliaItem::ControlItem* motor_state = getControlItem("Motor_State");
    GangliaState ret_value;
    std::vector<uint16_t> dest(motor_state->data_length);
    if (readInputRegisters(id, motor_state->address, motor_state->data_length, (uint16_t *)&dest[0]))
    {
        ret_value.encoder = dest[0] | (dest[1] << 16);
        ret_value.velocity = dest[2] | (dest[3] << 16);
        ret_value.current = dest[4] | (dest[5] << 16);

        if (read_abs_encoder)
        {
            const GangliaItem::ControlItem* absolute_encoder = getControlItem("Absolute_Encoder");
            if (readInputRegisters(id, absolute_encoder->address, absolute_encoder->data_length, (uint16_t *)&dest[0]))
            {
                ret_value.encoder_abs = dest[0];
            }
            else
            {
                ret = false;
            }
        }
    }
    else
    {
        ret = false;
    }
    *state = ret_value;
    return ret;
}

// bool GangliaControl::setCurrentPID(uint8_t id, uint32_t p, uint32_t i, uint32_t d);
// bool GangliaControl::setVelocityPID(uint8_t id, uint32_t p, uint32_t i, uint32_t d);
// bool GangliaControl::setPositionPID(uint8_t id, uint32_t p, uint32_t i, uint32_t d);

bool GangliaControl::initControlTable(uint32_t model_number)
{
    control_table_              = GangliaItem::getControlTable(model_number);
    the_number_of_control_item_ = GangliaItem::getTheNumberOfControlItem();
    model_info_                 = GangliaItem::getModelInfo(model_number);

    if (control_table_ == NULL || model_info_ == NULL)
    {
    //   if (log != NULL)
    //     *log = "[GangliaControl] Failed to get control table or model info";
      return false;
    }

    return true;
}

const GangliaItem::ControlItem *GangliaControl::getControlItem(const char *item_name)
{
    const GangliaItem::ControlItem *control_item = control_table_;
    uint8_t name_length = strlen(item_name);

    for (uint8_t num = 0; num < the_number_of_control_item_; num++)
    {
        if ((name_length == control_item->item_name_length) &&
            (memcmp(item_name, control_item->item_name, name_length) == 0))
        {
            return control_item;
        }
        control_item++;
    }

    // if (log != NULL)
    //     *log = "[GangliaControl] Can't find Item";
    return NULL;
}

bool GangliaControl::z_setVelovityMode(uint8_t id)
{
    ROS_INFO("set velocity mode");
    const GangliaItem::ControlItem* control_mode = getControlItem("Control_Mode");
    uint16_t value = 0x0003;
    bool temp = true; 
    ROS_INFO("id %i", id);
    ROS_INFO("value %04x", value);
    int address_temp;
    address_temp = control_mode->address;
    ROS_INFO("address %04x", address_temp);
    
    temp = writeRegister(id, control_mode->address, value);
    return temp;
}

bool GangliaControl::z_setAccTime(uint8_t id, uint16_t value)
{
    const GangliaItem::ControlItem* acc_time_l = getControlItem("Acceleration_Time_L");
    const GangliaItem::ControlItem* acc_time_r = getControlItem("Acceleration_Time_R");

    return writeRegister(id, acc_time_l->address, value) && writeRegister(id, acc_time_r->address, value);
}

bool GangliaControl::z_setDecTime(uint8_t id, uint16_t value)
{
    const GangliaItem::ControlItem* dec_time_l = getControlItem("Dcceleration_Time_L");
    const GangliaItem::ControlItem* dec_time_r = getControlItem("Dcceleration_Time_R");

    return writeRegister(id, dec_time_l->address, value) && writeRegister(id, dec_time_r->address, value);
}

bool GangliaControl::z_setEnable(uint8_t id)
{
    ROS_INFO("set enable");
    const GangliaItem::ControlItem* control_mode = getControlItem("Control_Word");
    uint16_t value = 0x0008;
    bool temp = true; 
    ROS_INFO("id %i", id);
    int address_temp;
    address_temp = control_mode->address;
    ROS_INFO("address %04x", address_temp);

    temp = writeRegister(id, control_mode->address, value);
    return temp;
}

bool GangliaControl::z_setVelocityTarget_L(uint8_t id, int16_t value)
{
    const GangliaItem::ControlItem* target_vel = getControlItem("Target_Velocity_L");

    return writeRegister(id, target_vel->address, value);
}

bool GangliaControl::z_setVelocityTarget_R(uint8_t id, int16_t value)
{
    const GangliaItem::ControlItem* target_vel = getControlItem("Target_Velocity_R");

    return writeRegister(id, target_vel->address, value);
}

bool GangliaControl::z_stop(uint8_t id)
{
    const GangliaItem::ControlItem* control_mode = getControlItem("Control_Word");
    uint16_t value = 0x0007;
    bool temp = true; 
    ROS_INFO("id %i", id);
    int address_temp;
    address_temp = control_mode->address;
    ROS_INFO("address %04x", address_temp);

    temp = writeRegister(id, control_mode->address, value);
    return temp;
}

bool GangliaControl::z_start(uint8_t id)
{
    ROS_INFO("z_start \n");
    return true;
}

bool GangliaControl::z_getVelocity(uint8_t id, int16_t* dest)
{
    const GangliaItem::ControlItem* actual_vel = getControlItem("Actual_Velocity_L");
    uint16_t buffer;
    bool ret = readRegisters(id, actual_vel->address, 2 ,&buffer);
    *dest = 0xFFFF && buffer;
    return ret;
}

bool GangliaControl::z_getCurrent (uint8_t id, int16_t* dest)
{
    const GangliaItem::ControlItem* actual_cur = getControlItem("Actual_Current_L");
    uint16_t buffer;
    bool ret = readRegisters(id, actual_cur->address, 1 ,&buffer);
    *dest = 0xFFFF && buffer;
    return ret;
}

bool GangliaControl::z_getMotorTemp (uint8_t id, int8_t* dest)
{
    uint16_t* dest_buffer;
    bool ret = false;
    const GangliaItem::ControlItem* motor_temp = getControlItem("Motor_Temp");
    ret = readRegisters(id, motor_temp->address, 1, dest_buffer);
    *(dest) = *dest_buffer >> 8;
    *(dest + 1) = *dest_buffer && 0xFF;
    return ret;
}

bool GangliaControl::z_getDriverTemp (uint8_t id, int16_t* dest)
{
    const GangliaItem::ControlItem* driver_temp = getControlItem("Driver_Temp");
    uint16_t buffer;
    bool ret = readRegisters(id, driver_temp->address,1,&buffer);
    *dest = 0xFFFF && buffer;
    return ret;
}

bool GangliaControl::z_checkALM (uint8_t id, uint16_t* dest)
{
    const GangliaItem::ControlItem* check_alm = getControlItem("Check_ALM_L");
    uint16_t buffer;
    bool ret = readRegisters(id, check_alm->address, 1, &buffer);
    *dest = 0xFFFF && buffer;
    return ret;
}