
// void testCoils(HgrModbusAscii  &modbus_, bool is_write, bool is_read, int skip=1)
// {
//     ros::Rate rate(100);
//     uint8_t src8[1000];
//     uint8_t dest8[1000];
//     uint16_t src16[1000];
//     uint16_t dest16[1000];

//     int memsize = 601;
//     std::cout << "memsize : " << memsize << std::endl;
//     for(int i_addr = 0; i_addr < 110; i_addr+=skip)
//     {
//         for(int i = 1; i < 110; i+=skip)
//         {
//             for(int j = 0; j < 1000; j++)
//                 src8[j] = rand() & 0x01;
//             for(int j = 0; j < 1000; j++)
//                 src16[j] = rand();

//             if(is_write)
//             {
//                 int nb = modbus_.writeMultipleCoils(i_addr, i, src8);
//                 // usleep(100*1000);
//                 rate.sleep();

//                 if( nb < 0 || nb != i)
//                     std::cout << "write error " << i_addr << " " << nb << "/" << i << std::endl;
//                 else if(is_read)
//                 {
//                     int nb = modbus_.readCoils(i_addr, i, dest8);
//                     // usleep(10*1000);
//                     rate.sleep();
        
//                     if( nb < 0 )
//                         std::cout << "read error  " << i_addr << " " << nb << "/" << i << std::endl;
//                     else
//                     {
//                         std::cout << "readCoils(addr = " << i_addr << ", " << nb << "/" << i << " : ";
//                         for(int j = 0; j < i; j++)
//                         {
//                             // std::cout << dest16[j] << " ";
//                             if( src8[j] != dest8[j])
//                                 std::cout << "!";
//                         }
//                         std::cout << std::endl;
//                     }
//                 }                
//             }
//         }
//     }
// }

// void testDiscreteInput(HgrModbusAscii  &modbus_, int skip=1)
// {
//     ros::Rate rate(100);
//     uint8_t src8[1000];
//     uint8_t dest8[1000];
//     uint16_t src16[1000];
//     uint16_t dest16[1000];

//     int memsize = 601;
//     std::cout << "memsize : " << memsize << std::endl;
//     for(int i_addr = 0; i_addr < 65; i_addr+=skip)
//     {
//         for(int i = 1; i < 65; i+=skip)
//         {
//             for(int j = 0; j < 1000; j++)
//                 src8[j] = rand() & 0x01;
//             for(int j = 0; j < 1000; j++)
//                 src16[j] = rand();

//             int nb = modbus_.readDiscreateInputs(i_addr, i, dest8);
//             // usleep(10*1000);
//             rate.sleep();

//             if( nb < 0 )
//                 std::cout << "read error  " << i_addr << " " << nb << "/" << i << std::endl;
//             else
//             {
//                 std::cout << "readDiscreateInputs(addr = " << i_addr << ", " << nb << "/" << i << " : ";
//                 for(int j = 0; j < i; j++)
//                     std::cout << (int)dest8[j] << " ";
//                 std::cout << std::endl;
//             }
//         }                
//     }
// }

// void testHoldingRegisters(HgrModbusAscii  &modbus_, bool is_write, bool is_read, int skip=1)
// {
//     ros::Rate rate(100);
//     uint8_t src8[1000];
//     uint8_t dest8[1000];
//     uint16_t src16[1000];
//     uint16_t dest16[1000];

//     int memsize = 601;
//     std::cout << "memsize : " << memsize << std::endl;
//     for(int i_addr = 0; i_addr < 65; i_addr+=skip)
//     {
//         for(int i = 1; i < 65; i+=skip)
//         {
//             for(int j = 0; j < 1000; j++)
//                 src8[j] = rand() & 0x01;
//             for(int j = 0; j < 1000; j++)
//                 src16[j] = rand();

//             if(is_write)
//             {
//                 int nb = modbus_.writeMultipleRegisters(i_addr, i, src16);
//                 // usleep(100*1000);
//                 rate.sleep();

//                 if( nb < 0 || nb != i)
//                     std::cout << "write error " << i_addr << " " << nb << "/" << i << std::endl;
//                 else if(is_read)
//                 {
//                     int nb = modbus_.readHoldingRegisters(i_addr, i, dest16);
//                     // usleep(10*1000);
//                     rate.sleep();
        
//                     if( nb < 0 )
//                         std::cout << "read error  " << i_addr << " " << nb << "/" << i << std::endl;
//                     else
//                     {
//                         std::cout << "readHoldingRegisters(addr = " << i_addr << ", " << nb << "/" << i << " : ";
//                         for(int j = 0; j < i; j++)
//                         {
//                             // std::cout << dest16[j] << " ";
//                             if( src16[j] != dest16[j])
//                                 std::cout << "!";
//                         }
//                         std::cout << std::endl;
//                     }
//                 }                
//             }
//         }
//     }
// }

// void testInputRegisters(HgrModbusAscii  &modbus_, int skip=1)
// {
//     ros::Rate rate(100);
//     uint8_t src8[1000];
//     uint8_t dest8[1000];
//     uint16_t src16[1000];
//     uint16_t dest16[1000];

//     int memsize = 601;
//     std::cout << "memsize : " << memsize << std::endl;
//     for(int i_addr = 0; i_addr < 65; i_addr+=skip)
//     {
//         for(int i = 1; i < 65; i+=skip)
//         {
//             for(int j = 0; j < 1000; j++)
//                 src8[j] = rand() & 0x01;
//             for(int j = 0; j < 1000; j++)
//                 src16[j] = rand();

//             int nb = modbus_.readInputRegisters(i_addr, i, dest16);
//             // usleep(10*1000);
//             rate.sleep();

//             if( nb < 0 )
//                 std::cout << "read error  " << i_addr << " " << nb << "/" << i << std::endl;
//             else
//             {
//                 std::cout << "readInputRegisters(addr = " << i_addr << ", " << nb << "/" << i << " : ";
//                 for(int j = 0; j < i; j++)
//                     std::cout << dest16[j] << " ";
//                 std::cout << std::endl;
//             }
//         }                
//     }
// }

// void testSingleCoil(HgrModbusAscii  &modbus_, bool is_write, bool is_read, int skip=1)
// {
//     ros::Rate rate(100);
//     uint8_t src8[1000];
//     uint8_t dest8[1000];
//     uint16_t src16[1000];
//     uint16_t dest16[1000];

//     int memsize = 601;
//     std::cout << "memsize : " << memsize << std::endl;
//     for(int i_addr = 0; i_addr < 102; i_addr+=skip)
//     {
//         for(int i = 0; i < 3; i+=skip)
//         {
//             for(int j = 0; j < 1000; j++)
//                 src8[j] = rand() & 0x01;
//             for(int j = 0; j < 1000; j++)
//                 src16[j] = rand();

//             if(is_write)
//             {
//                 int nb = modbus_.writeSingleCoil(i_addr, src8[i_addr]);
//                 // usleep(100*1000);
//                 rate.sleep();

//                 if( nb < 0 || nb != 1)
//                     std::cout << "write error " << i_addr << ", iter " << i << std::endl;
//                 else if(is_read)
//                 {
//                     int nb = modbus_.readCoils(i_addr, 1, dest8+i_addr);
//                     // usleep(10*1000);
//                     rate.sleep();
        
//                     if( nb < 0 )
//                         std::cout << "read error  " << i_addr << ", iter" << i << std::endl;
//                     else
//                     {
//                         std::cout << "readCoils(addr = " << i_addr << ", iter " << i << " : ";
//                         // std::cout << (int)src8[i_addr] << "," << (int)dest8[i_addr] << " ";
//                         if( src8[i_addr] != dest8[i_addr])
//                             std::cout << "!";
//                         std::cout << std::endl;
//                     }
//                 }                
//             }
//         }
//     }
// }

// void testSingleRegister(HgrModbusAscii  &modbus_, bool is_write, bool is_read, int skip=1)
// {
//     ros::Rate rate(100);
//     uint8_t src8[1000];
//     uint8_t dest8[1000];
//     uint16_t src16[1000];
//     uint16_t dest16[1000];

//     int memsize = 601;
//     std::cout << "memsize : " << memsize << std::endl;
//     for(int i_addr = 0; i_addr < 102; i_addr+=skip)
//     {
//         for(int i = 0; i < 3; i+=skip)
//         {
//             for(int j = 0; j < 1000; j++)
//                 src8[j] = rand() & 0x01;
//             for(int j = 0; j < 1000; j++)
//                 src16[j] = rand();

//             if(is_write)
//             {
//                 int nb = modbus_.writeSingleRegister(i_addr, src16[i_addr]);
//                 // usleep(100*1000);
//                 rate.sleep();

//                 if( nb < 0 || nb != 1)
//                     std::cout << "write error " << i_addr << ", iter " << i << std::endl;
//                 else if(is_read)
//                 {
//                     int nb = modbus_.readHoldingRegisters(i_addr, 1, dest16+i_addr);
//                     // usleep(10*1000);
//                     rate.sleep();
        
//                     if( nb < 0 )
//                         std::cout << "read error  " << i_addr << ", iter" << i << std::endl;
//                     else
//                     {
//                         std::cout << "readHoldingRegisters(addr = " << i_addr << ", iter " << i << " : ";
//                         if( src16[i_addr] != dest16[i_addr])
//                             std::cout << "!";
//                         std::cout << std::endl;
//                     }
//                 }                
//             }
//         }
//     }
// }

// void testMask(HgrModbusAscii  &modbus_, bool is_write, bool is_read, int skip=1)
// {
//     ros::Rate rate(100);
//     uint8_t src8[1000];
//     uint8_t dest8[1000];
//     uint16_t src16[1000];
//     uint16_t dest16[1000];

//     int memsize = 601;
//     std::cout << "memsize : " << memsize << std::endl;
//     for(int i_addr = 0; i_addr < 102; i_addr+=skip)
//     {
//         for(int i = 0; i < 3; i+=skip)
//         {
//             for(int j = 0; j < 1000; j++)
//                 src8[j] = rand() & 0x01;
//             for(int j = 0; j < 1000; j++)
//                 src16[j] = rand();

//             if(is_write)
//             {
//                 int nb = modbus_.writeSingleRegister(i_addr, src16[i_addr]);
//                 rate.sleep();
//                 if( nb < 0 || nb != 1)
//                     std::cout << "write error " << i_addr << ", iter " << i << std::endl;

//                 nb = modbus_.maskWriteRegisters(i_addr, src16[i_addr+1], src16[i_addr+2]);
//                 rate.sleep();

//                 if( nb < 0 || nb != 1)
//                     std::cout << "mask error " << i_addr << ", iter " << i << std::endl;
//                 else if(is_read)
//                 {
//                     int nb = modbus_.readHoldingRegisters(i_addr, 1, dest16+i_addr);
//                     // usleep(10*1000);
//                     rate.sleep();
        
//                     uint16_t result = (src16[i_addr] & src16[i_addr+1]) | (src16[i_addr+2] & ~src16[i_addr+1]);
//                     if( nb < 0 )
//                         std::cout << "read error  " << i_addr << ", iter" << i << std::endl;
//                     else
//                     {
//                         std::cout << "readHoldingRegisters(addr = " << i_addr << ", iter " << i << " : ";
//                         if( src16[i_addr] != result)
//                             std::cout << "!";
//                         std::cout << std::endl;
//                     }
//                 }                
//             }
//         }
//     }
// }

// void testWriteRead(HgrModbusAscii  &modbus_, int skip=1)
// {
//     ros::Rate rate(100);
//     uint16_t src16[1000];
//     uint16_t buff_pre[1000];
//     uint16_t buff_post[1000];
//     uint16_t buff_result[1000];

//     for(int write_nb = 36; write_nb < 65; write_nb+=skip)
//     {
//         for(int read_nb = 0; read_nb < 65; read_nb+=skip)
//         {
//             for(int iter = 0; iter < 10; iter++)
//             {
//                 int write_addr = rand()%100;
//                 int read_addr = rand()%100;
//                 int rand_cnt = 0;

//                 while( (write_addr <= read_addr && read_addr < write_addr+write_nb) ||
//                     (read_addr <= write_addr && write_addr < read_addr+read_nb) ||
//                     write_addr+write_nb > 99 || read_addr+read_nb > 99 )
//                 {
//                     write_addr = rand()%100;
//                     read_addr = rand()%100;

//                     if( rand_cnt++ > 100 )
//                         break;
//                 }
//                 if( rand_cnt > 100 )
//                     continue;

//                 for(int j = 0; j < 1000; j++)
//                 {
//                     src16[j] = rand();
//                     buff_pre[j] = rand();
//                     buff_post[j] = rand();
//                     buff_result[j] = rand();
//                 }

//                 if( 0 > modbus_.readHoldingRegisters(0, 50, buff_pre) )
//                     std::cout << "read error : " << write_nb << " " << read_nb << std::endl;
//                 rate.sleep();
//                 if( 0 > modbus_.readHoldingRegisters(50, 50, buff_pre+50) )
//                     std::cout << "read error : " << write_nb << " " << read_nb << std::endl;
//                 rate.sleep();

//                 for(int i = 0; i < 1000; i++)
//                     buff_result[i] = buff_post[i] = buff_pre[i];

//                 int nb = modbus_.writeAndReadRegisters(write_addr, write_nb, src16+write_addr, read_addr, read_nb, buff_result+read_addr);
//                 if( nb < 0 || nb != read_nb )
//                     std::cout << "read/write error " << write_addr << "+" << write_nb << " " << read_addr << "+" << read_nb << " " << iter << std::endl;
//                 else
//                 {
//                     if( 0 > modbus_.readHoldingRegisters(0, 50, buff_post) )
//                         std::cout << "read error : " << write_nb << " " << read_nb << std::endl;
//                     rate.sleep();
//                     if( 0 > modbus_.readHoldingRegisters(50, 50, buff_post+50) )
//                         std::cout << "read error : " << write_nb << " " << read_nb << std::endl;
//                     rate.sleep();

//                     std::cout << "write " << write_nb << " " << read_nb << " " << iter << std::endl;
//                     for(int i = 0; i < write_nb; i++)
//                     {
//                         if( buff_post[write_addr+i] != src16[write_addr+i] )
//                             std::cout << "!("<< buff_post[write_addr+i] << " " << src16[write_addr+i] << ") ";
//                     }
//                     std::cout << std::endl;
//                     std::cout << "read " << write_nb << " " << read_nb << " " << iter << std::endl;
//                     for(int i = 0; i < read_nb; i++)
//                     {
//                         if( buff_result[read_addr+i] != buff_pre[read_addr+i] )
//                             std::cout << "!("<< buff_result[read_addr+i] << " " << buff_pre[read_addr+i] << " " << buff_post[read_addr+i] << ") ";
//                     }
//                     std::cout << std::endl;
//                 }
//             }
//         }
//     }
// }

// void testMultiNode(HgrModbusAscii  &modbus_, HgrModbusAscii *modbus_blanks, int skip=1)
// {
//     ros::Rate rate(1000);
//     uint16_t src16[1000];
//     uint16_t buff_pre[1000];
//     uint16_t buff_post[1000];
//     uint16_t buff_result[1000];

//     int write_addr = 0;//rand()%100;
//     int read_addr = 10;//rand()%100;
//     int rand_cnt = 0;

//     // if( 0 > modbus_.readHoldingRegisters(0, 50, buff_pre) )
//     //     std::cout << "read error : " << write_nb << " " << read_nb << std::endl;
//     // if( 0 > modbus_.readHoldingRegisters(50, 50, buff_pre+50) )
//     //     std::cout << "read error : " << write_nb << " " << read_nb << std::endl;
//     for(int write_nb = 36; write_nb < 65; write_nb+=skip)
//     {
//         for(int read_nb = 0; read_nb < 65; read_nb+=skip)
//         {
//             for(int iter = 0; iter < 10; iter++)
//             {

//                 for(int i = 0; i < 1000; i++)
//                     buff_result[i] = buff_post[i] = buff_pre[i];

//                 // for(int i = 0; i < 15; i++)
//                 //     modbus_blanks[i].writeAndReadRegisters(write_addr, 8, src16+write_addr, read_addr, 8, buff_result+read_addr);

//                 int nb = modbus_.writeAndReadRegisters(write_addr, 8, src16+write_addr, read_addr, 8, buff_result+read_addr);
//                 if( nb < 0 || nb != read_nb )
//                     std::cout << "read/write error " << write_addr << "+" << 8 << " " << read_addr << "+" << 8 << " " << iter << std::endl;
//                 // else
//                 // {
//                 //     if( 0 > modbus_.readHoldingRegisters(0, 50, buff_post) )
//                 //         std::cout << "read error : " << write_nb << " " << read_nb << std::endl;
//                 //     if( 0 > modbus_.readHoldingRegisters(50, 50, buff_post+50) )
//                 //         std::cout << "read error : " << write_nb << " " << read_nb << std::endl;

//                 //     // std::cout << "write " << write_nb << " " << read_nb << " " << iter << std::endl;
//                 //     for(int i = 0; i < write_nb; i++)
//                 //     {
//                 //         if( buff_post[write_addr+i] != src16[write_addr+i] )
//                 //             std::cout << "!("<< buff_post[write_addr+i] << " " << src16[write_addr+i] << ") ";
//                 //     }
//                 //     // std::cout << std::endl;
//                 //     // std::cout << "read " << write_nb << " " << read_nb << " " << iter << std::endl;
//                 //     for(int i = 0; i < read_nb; i++)
//                 //     {
//                 //         if( buff_result[read_addr+i] != buff_pre[read_addr+i] )
//                 //             std::cout << "!("<< buff_result[read_addr+i] << " " << buff_pre[read_addr+i] << " " << buff_post[read_addr+i] << ") ";
//                 //     }
//                 //     // std::cout << std::endl;
//                 // }
//                 rate.sleep();
//                 std::cout << "rate.cycleTime() : " << rate.cycleTime() << std::endl;
//             }
//         }
//     }
// }

// void testModbus(const std::string &port, uint32_t baud)
// {
//     serial::Serial  serial_;
//     serial_.setPort(port);
//     serial_.setBaudrate(baud);
//     serial_.setTimeout(0, 10, 0, 10, 0); // should be 50
//     if( !serial_.isOpen() )
//         serial_.open();


//     if( !serial_.isOpen() )
//     {
//         std::cout << "can't open serial port" << std::endl;
//         return;
//     }

//     HgrModbusAscii  modbus_;    // Serial port from Serial Lib
//     modbus_.setSerial(&serial_);
//     modbus_.setSlaveAddress(0x11);

//     HgrModbusAscii  modbus_blanks[15];    // Serial port from Serial Lib
//     for(int i = 0; i < 15; i++)
//     {
//         modbus_blanks[i].setSerial(&serial_);
//         modbus_blanks[i].setSlaveAddress(0x12+i);
//     }

//     // testCoils(modbus_, true, true); // need more than 100 coils
//     // testDiscreteInput(modbus_);
//     // testHoldingRegisters(modbus_, true, true);
//     // testInputRegisters(modbus_);
//     // testSingleCoil(modbus_, true, true); // need more than 100 coils
//     // testSingleRegister(modbus_, true, true);
//     // testMask(modbus_, true, true); // response with MODBUS_EXCEPTION_ILLEGAL_FUNCTION
//     // testWriteRead(modbus_);
//     testMultiNode(modbus_, modbus_blanks);
// }
